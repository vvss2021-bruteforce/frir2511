package it;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import model.*;
import org.mockito.Mockito;
import repository.InventoryRepository;
import service.InventoryService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.fail;

public class RepositoryIntegration {

    private final static Path FILE_PATH = Path.of("mock_items.txt");
    private Path tempPath;
    private InventoryRepository repo;
    private InventoryService service;

    @BeforeEach
    void setUp() throws IOException {
        tempPath = Files.createTempFile(Path.of("."), "mock_", ".txt");

        repo = new InventoryRepository(tempPath.toFile());
        service = new InventoryService(repo);
    }

    @AfterEach
    void tearDown() throws IOException {
        Files.deleteIfExists(tempPath);
    }

    @Test
    public void addProductValidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        Part dummyOk = Mockito.mock(OutsourcedPart.class);
        Mockito.when(dummyOk.getName()).thenReturn("dummy name");
        Mockito.when(dummyOk.getPartId()).thenReturn(1000);
        Mockito.when(dummyOk.getPrice()).thenReturn(10.3);
        Mockito.when(dummyOk.getInStock()).thenReturn(5);
        Mockito.when(dummyOk.getMin()).thenReturn(5);
        Mockito.when(dummyOk.getMax()).thenReturn(50);
        try {
            Validator.validateProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            assert true;
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
        repo.addPart(dummyOk);
        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            assert(service.getAllProducts().size() == 1);
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
    }

    @Test
    public void addProductInvalidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        Part dummyOk1 = Mockito.mock(OutsourcedPart.class);
        Mockito.when(dummyOk1.getName()).thenReturn("dummy name");
        Mockito.when(dummyOk1.getPartId()).thenReturn(1000);
        Mockito.when(dummyOk1.getPrice()).thenReturn(10.3);
        Mockito.when(dummyOk1.getInStock()).thenReturn(5);
        Mockito.when(dummyOk1.getMin()).thenReturn(5);
        Mockito.when(dummyOk1.getMax()).thenReturn(50);

        Part dummyOk = Mockito.mock(OutsourcedPart.class);
        Mockito.when(dummyOk.getName()).thenReturn("dummy name");
        Mockito.when(dummyOk.getPartId()).thenReturn(1001);
        Mockito.when(dummyOk.getPrice()).thenReturn(100.7);
        Mockito.when(dummyOk.getInStock()).thenReturn(5);
        Mockito.when(dummyOk.getMin()).thenReturn(5);
        Mockito.when(dummyOk.getMax()).thenReturn(50);

        repo.addPart(dummyOk);
        repo.addPart(dummyOk1);
        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk, dummyOk1));
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(service.getAllProducts().size() == 0);
        }
    }
}
