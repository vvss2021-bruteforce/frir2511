package it;

import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import model.*;
import org.mockito.Mockito;
import repository.InventoryRepository;
import service.InventoryService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.fail;

public class DomainIntegration {

    private final static Path FILE_PATH = Path.of("mock_items.txt");
    private Path tempPath;
    private InventoryRepository repo;
    private InventoryService service;

    @BeforeEach
    void setUp() throws IOException {
        tempPath = Files.createTempFile(Path.of("."), "mock_", ".txt");

        repo = new InventoryRepository(tempPath.toFile());
        service = new InventoryService(repo);
    }

    @AfterEach
    void tearDown() throws IOException {
        Files.deleteIfExists(tempPath);
    }


    @Test
    public void addPartValidTest() throws ValidationException {
        // int partId, String name, double price, int inStock, int min, int max, String companyName
        Part dummyOk = new OutsourcedPart(1, "name", 20.3, 10, 10, 100, "company name");
        try {
            Validator.validatePart("name", 20.3, 10, 10, 100);
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
        service.addOutsourcePart("name", 20.3, 10, 10, 100, "company name");
        System.out.println(service.getAllParts().size());
        assert(service.getAllParts().size() == 1);
        assert(service.getAllParts().contains(dummyOk));
    }

    @Test
    public void addPartInvalidTest() {
        // int partId, String name, double price, int inStock, int min, int max, String companyName
        try {
            Validator.validatePart("name", -20.3, 10, 10, 100);
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
        }
        try {
            service.addOutsourcePart("name", -20.3, 10, 10, 100, "company name");
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
            assert(service.getAllParts().size() == 0);
        }
    }

    @Test
    public void addProductValidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        Part dummyOk = new OutsourcedPart(1, "name", 20.3, 10, 10, 100, "company name");
        Product product = new Product(1, "name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
        try {
            Validator.validateProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            assert true;
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
        try {
            service.addOutsourcePart("name", 20.3, 10, 10, 10, "company name");
        } catch (ValidationException e) {
            assert(false);
        }

        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            assert(service.getAllProducts().size() == 1);
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
    }

    @Test
    public void addProductInvalidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        try {
            Validator.validateProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList());
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
        }
        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList());
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
            assert(service.getAllProducts().size() == 0);
        }
    }
}
