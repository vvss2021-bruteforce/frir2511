package unit.service;
import model.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import service.InventoryService;

import static org.junit.jupiter.api.Assertions.fail;

public class InventoryServiceMocksTest {

    private AutoCloseable closeable;

    @Mock
    private InventoryRepository repo;

    @InjectMocks
    private InventoryService service;

    @BeforeEach
    public void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    public void addPartValidTest() throws ValidationException {
        // int partId, String name, double price, int inStock, int min, int max, String companyName
        Part dummyOk = new OutsourcedPart(1, "name", 20.3, 10, 10, 100, "company name");
        try {
            Validator.validatePart("name", 20.3, 10, 10, 100);
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
        service.addOutsourcePart("name", 20.3, 10, 10, 100, "company name");
        Mockito.when(repo.getAllParts()).thenReturn(FXCollections.observableArrayList(dummyOk));
        assert(service.getAllParts().contains(dummyOk));
        assert(service.getAllParts().size() == 1);
    }

    @Test
    public void addPartInvalidTest() {
        // int partId, String name, double price, int inStock, int min, int max, String companyName
        try {
            Validator.validatePart("name", -20.3, 10, 10, 100);
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
        }
        try {
            service.addOutsourcePart("name", -20.3, 10, 10, 100, "company name");
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
            Mockito.when(repo.getAllParts()).thenReturn(null);
            assert(service.getAllParts() == null);
        }
    }

    @Test
    public void addProductValidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        Part dummyOk = new OutsourcedPart(1, "name", 20.3, 10, 10, 100, "company name");
        Product product = new Product(1, "name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
        try {
            Validator.validateProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            assert true;
        } catch (ValidationException e) {
            fail("Should not have thrown exception!");
        }
        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList(dummyOk));
            fail("Should not have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
            Mockito.when(repo.getAllProducts()).thenReturn(FXCollections.observableArrayList(product));
            assert(service.getAllProducts().equals(FXCollections.observableArrayList(product)));
        }
    }

    @Test
    public void addProductInvalidTest() {
        // String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts
        try {
            Validator.validateProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList());
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
        }
        try {
            service.addProduct("name", 20.3, 10, 10, 100, FXCollections.observableArrayList());
            fail("Should have thrown exception!");
        } catch (ValidationException e) {
            assert(true);
            Mockito.when(repo.getAllProducts()).thenReturn(null);
            assert(service.getAllProducts() == null);
        }
    }
}
