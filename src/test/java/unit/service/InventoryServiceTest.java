package unit.service;
import model.InhousePart;
import model.OutsourcedPart;
import model.Part;
import model.ValidationException;
import repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import service.InventoryService;

import java.io.File;

class InventoryServiceTest {
    private InventoryRepository repo;
    private InventoryService service;

    @BeforeEach
    void setUp() {
        try {
            File file = new File("src/test/resources/data/items.txt");
            System.out.println(file.getAbsolutePath());
            repo = new InventoryRepository(file);
            service = new InventoryService(repo);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Test
    @Order(1)
    void addInhousePartWithEmptyName() {
        assert true;
        try {
            service.addInhousePart("", 1.00, 1, 1, 1, 2);
            assert false;
        } catch (ValidationException e) {
            //e.printStackTrace();
        }
    }

    @Test
    @Order(2)
    void addInHousePartWithAllValidFields() {
        assert true;
        try {
            service.addOutsourcePart("Ciocoflender", 9999.999, 10000, 1, 1000000, "Florin Piersic");
        } catch (ValidationException e) {
            assert false;
        }
    }

    @Test
    @Order(3)
    void addInHousePartWithValidInStock() {
        try {
            service.addInhousePart("Nume de piesa", 20, 5, 1, 10, 2);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    /*
        inStock cannot be negative
     */
    @Order(4)
    void addInHousePartWithInvalidInStock1() {
        try {
            service.addInhousePart("Alt nume de piesa", 20, -5, 1, 10, 2);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    /*
        inStock must be greater than min
     */
    @Order(5)
    void addInHousePartWithInvalidInStock2() {
        try {
            service.addInhousePart("Alt numee de piesa", 20, 5, 9, 10, 2);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    /*
        inStock must be smaller than max
     */
    @Order(6)
    void addInHousePartWithInvalidInStock3() {
        try {
            service.addInhousePart("Alt numeee de piesa", 20, 50, 9, 10, 2);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }


    @Test
    /*
        min must be <= max
     */
    @Order(7)
    void addInHousePartWithInvalidInStock4() {
        try {
            service.addInhousePart("O noua piesa", 20, 50, 100, 10, 2);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }

    @Test
    @Order(8)
    void addInhousePartWithValidBoundaryValues() {
        try {
            service.addInhousePart("Ciocolata Laura", 0.01, 1, 1, 1, 2);
        } catch (Exception e) {
            assert false;
        }
    }

    @Test
    @Order(9)
    void addInhousePartWithInvalidBoundaryPrice() {
        try {
            service.addInhousePart("motor", 0.00999999999999999, 10, 1, 1000, 3);
            assert false;
        } catch (ValidationException e) {
            //
        }
    }

    @Test
    @Order(10)
    void addProductWithNoParts() {
        try {
            service.addProduct("laptop", 2000.0, 10, 1, 10, FXCollections.emptyObservableList());
            assert false;
        } catch (ValidationException e) {
            //
        }
    }

    @Test
    @Order(11)
    void addProductWithPriceEqualWithSumOfParts() {
        try {

            Part part1 = service.lookupPart("Ciocolata Laura");
            Part part2 = service.lookupPart("Ciocoflender");
            ObservableList<Part> parts = FXCollections.observableArrayList(part1, part2);

            service.addProduct("Ciococeptie", 10000.009, 100, 1, 10000, parts);
        } catch (ValidationException e) {
            e.printStackTrace();
            assert false;
        }
    }

    @Test
    @Order(12)
    void addProductWithNonexistentPartsInRepo() {
        try {
            Part part1 = new InhousePart(100, "Ciocolata Laura", 0.001, 1, 1, 1, 2);
            Part part2 = new OutsourcedPart(101, "Ciocoflender", 9999.999, 10000, 1, 1000000, "FlorinPiersic");
            ObservableList<Part> parts = FXCollections.observableArrayList(part1, part2);

            service.addProduct("Ciococeptie", 10000, 100, 1, 10000, parts);
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }
}