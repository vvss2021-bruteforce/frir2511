package unit.model;

import javafx.collections.FXCollections;
import model.OutsourcedPart;
import model.Part;
import model.Product;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

class ProductTest {
    private static Product product;

    @BeforeAll
    public static void setUp() {
        product  = new Product(1, "produs magic", 17, 5, 10, 100, FXCollections.observableArrayList());
    }

    @Test
    @Order(1)
    void testGetName() {
        assert product.getName().equals("produs magic");
    }

    @Test
    @Order(2)
    void testSetName() {
        product.setName("produs miraculos");
        assert(product.getName().equals("produs miraculos"));

    }

    @Test
    @Order(3)
    void testNullLookupParts(){
        Part nullProduct = product.lookupAssociatedPart("random");
        assert nullProduct == null;
    }

    @Test
    @Order(4)
    void testLookupParts(){
        OutsourcedPart part1=new OutsourcedPart(1,"part1",12,5,4,10,"com1");
        OutsourcedPart part2=new OutsourcedPart(2,"part2",15,5,4,10,"com4");
        OutsourcedPart part3=new OutsourcedPart(3,"part3",100,5,4,10,"com2");
        product.addAssociatedPart(part1);
        product.addAssociatedPart(part2);
        product.addAssociatedPart(part3);
        assert(product.lookupAssociatedPart("part1").equals(part1));
    }
    @Test
    @Order(5)
    void testRemoveParts(){
        OutsourcedPart part1=new OutsourcedPart(4,"part4",12,5,4,10,"com1");
        OutsourcedPart part2=new OutsourcedPart(5,"part5",15,5,4,10,"com4");
        OutsourcedPart part3=new OutsourcedPart(6,"part6",100,5,4,10,"com2");
        product.addAssociatedPart(part1);
        product.addAssociatedPart(part2);
        product.addAssociatedPart(part3);
        product.removeAssociatedPart(part2);
        assert(product.lookupAssociatedPart("part1")==null);

    }





}