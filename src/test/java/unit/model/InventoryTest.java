package unit.model;
import javafx.collections.FXCollections;
import model.Inventory;
import model.Product;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class InventoryTest {
    private static Inventory inventory;

    private static Inventory mockitoInventory;

    @BeforeAll
    public static void setUp() {
        inventory  = new Inventory();
        inventory.addProduct(new Product(1, "product5", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
        inventory.addProduct(new Product(2, "product6", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
        inventory.addProduct(new Product(3, "product7", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
        inventory.addProduct(new Product(4, "product8", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
    }

    @BeforeEach
    public void setUpMockito() {
        mockitoInventory = new Inventory();
        Product mockProduct = Mockito.mock(Product.class);
        Mockito.when(mockProduct.getProductId()).thenReturn(1234);
        Mockito.when(mockProduct.getName()).thenReturn("Music makes me free");
        Product otherMockProduct = Mockito.mock(Product.class);
        Mockito.when(otherMockProduct.getProductId()).thenReturn(1235);
        Mockito.when(otherMockProduct.getName()).thenReturn("Guma de mestecat");
        mockitoInventory.addProduct(mockProduct);
        mockitoInventory.addProduct(otherMockProduct);
    }

    @Test
    public void testSearchProductByID() {
        assert inventory.lookupProduct("1").equals(new Product(1, "product5", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
    }

    @Test
    /*
        lista de produse nu e vida si elementul cautat dupa nume este gasit
     */
    public void testSearchProductByNameValidIncompleteName() {
        assert inventory.lookupProduct("product").equals(new Product(1, "product5", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
    }

    @Test
    public void testSearchProductByNameValidCompleteName() {
        assert inventory.lookupProduct("product5").equals(new Product(1, "product5", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
    }

    @Test
    public void testSearchProductByEmptyName() {
        Product firstProduct = inventory.lookupProduct("");
        assert firstProduct.equals(new Product(1, "product5", 24.6, 10, 9, 20, FXCollections.observableArrayList()));
    }


    @Test
    /*
        lista de produse este vida sau produsul cautat dupa nume nu se afla in lista
     */
    public void testSearchProductByNameInvalid() {
        Product nullProduct = inventory.lookupProduct("random");
        assert nullProduct == null;
    }

    @Test
    public void testSearchProductWithEmptyInventory() {
        Inventory emptyInventory = new Inventory();
        assert emptyInventory.lookupProduct("random string").equals(new Product(0, null, 0.0, 0, 0, 0, null));
    }

    @Test
    public void testAddProductWithMockito() {
        Product mockProduct = Mockito.mock(Product.class);
        int initialSize = mockitoInventory.getProducts().size();
        mockitoInventory.addProduct(mockProduct);
        assert mockitoInventory.getProducts().size() == initialSize+1;
    }

    @Test
    public void testLookupProductByNameWithMockito() {
        assert mockitoInventory.lookupProduct("Music").getName().equals("Music makes me free");
    }

    @Test
    public void testLookupProductByIdWithMockito() {
        assert mockitoInventory.lookupProduct("1234").getProductId() == 1234;
    }

    @Test
    public void testLookupProductByNameWithValidOrderWithMockito() {
        assert mockitoInventory.lookupProduct("m", 2).getProductId() == 1235;
    }

    @Test
    public void testLookupProductByNameWithInvalidOrderWithMockito() {
        assert mockitoInventory.lookupProduct("m", -1) == null;
    }



}
