package unit.repository;

import model.InhousePart;
import model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import repository.InventoryRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InventoryRepositoryTest {

    private final static Path FILE_PATH = Path.of("mock_items.txt");
    private static InventoryRepository repository;
    private static InhousePart part;

    @BeforeAll
    static void setUp() throws IOException {
        Files.deleteIfExists(FILE_PATH);
        Files.createFile(FILE_PATH);

        repository = new InventoryRepository(FILE_PATH.toFile());
        part = Mockito.mock(InhousePart.class);
        Mockito.when(part.getPartId()).thenReturn(1);
        Mockito.when(part.getPrice()).thenReturn(100.0);
        Mockito.when(part.getInStock()).thenReturn(100);
        Mockito.when(part.getName()).thenReturn("Ciocolata Laura");
        Mockito.when(part.getMax()).thenReturn(101);
        Mockito.when(part.getMin()).thenReturn(1);
        Mockito.when(part.getMachineId()).thenReturn(111);
        Mockito.when(part.toString()).thenReturn("I,1,Ciocolata Laura,100.0,100,1,101,111");
        repository.addPart(part);
    }

    @AfterAll
    static void tearDown() throws IOException {
        Files.deleteIfExists(FILE_PATH);
    }

    @Test
    @Order(2)
    public void testAddProduct() {
        Product mockProduct = Mockito.mock(Product.class);
        //Mockito.when(mockProduct.)
        Mockito.when(mockProduct.getProductId()).thenReturn(2);
        Mockito.when(mockProduct.getName()).thenReturn("Tort");
        Mockito.when(mockProduct.getInStock()).thenReturn(1);
        Mockito.when(mockProduct.getMin()).thenReturn(1);
        Mockito.when(mockProduct.getMax()).thenReturn(1);
        Mockito.when(mockProduct.getPrice()).thenReturn(200.0);
        Mockito.when(mockProduct.getAssociatedParts()).thenReturn(FXCollections.observableArrayList(part));
        Mockito.when(mockProduct.toString()).thenReturn("P,2,Tort,200.0,1,1,1");

        ObservableList<Product> products = repository.getAllProducts();
        int initialSize = products.size();
        repository.addProduct(mockProduct);
        assert products.size() == initialSize+1;
        assert products.get(products.size()-1).getName().equals("Tort");
        assert products.get(products.size()-1).getProductId() == 2;
    }

    @Test
    @Order(1)
    public void testLookupProductWithEmptyRepository() {
        Product p = repository.lookupProduct("t");
        assert p.getProductId() == 0;
        assert p.getName() == null;
    }

    @Test
    @Order(3)
    public void testLookupProductWithValidName() {
        assert repository.lookupProduct("t").getName().equals("Tort");
    }

    @Test
    @Order(3)
    public void testLookupProductWithInvalidName() {
        assert repository.lookupProduct("x") == null;
    }

    @Test
    @Order(3)
    public void testLookupProductWithInvalidIndex() {
        assert repository.lookupProduct("t", 1000) == null;
    }

    @Test
    @Order(4)
    public void testUpdateProduct() {
        Product mockProduct = Mockito.mock(Product.class);
        //Mockito.when(mockProduct.)
        Mockito.when(mockProduct.getProductId()).thenReturn(5);
        Mockito.when(mockProduct.getName()).thenReturn("Frigider");
        Mockito.when(mockProduct.getInStock()).thenReturn(1);
        Mockito.when(mockProduct.getMin()).thenReturn(1);
        Mockito.when(mockProduct.getMax()).thenReturn(1);
        Mockito.when(mockProduct.getPrice()).thenReturn(2000.0);
        Mockito.when(mockProduct.getAssociatedParts()).thenReturn(FXCollections.observableArrayList(part));
        Mockito.when(mockProduct.toString()).thenReturn("P,2,Frigider,200.0,1,1,1");
        repository.updateProduct(0, mockProduct);
        Product p = repository.getAllProducts().get(0);
        assert p.getProductId() == 5;
        assert p.getName().equals("Frigider");
    }

    @Test
    @Order(5)
    public void testDeleteProduct() {
        int initialSize = repository.getAllProducts().size();
        repository.deleteProduct(repository.getAllProducts().get(0));
        assert repository.getAllProducts().size() == initialSize - 1;
    }

    @Test
    @Order(6)
    public void testDeletePart() {
        int initialSize = repository.getAllParts().size();
        repository.deletePart(part);
        assert repository.getAllParts().size() == initialSize - 1;
    }
}