package service;

import model.*;
import repository.InventoryRepository;
import javafx.collections.ObservableList;

public class InventoryService {

    private InventoryRepository repo;

    public InventoryService(InventoryRepository repo){
        this.repo =repo;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue) throws ValidationException {
        Validator.validatePart(name, price, inStock, min, max);
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue) throws ValidationException {
        Validator.validatePart(name, price, inStock, min, max);
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts) throws ValidationException {
        Validator.validateProduct(name, price, inStock, min, max, addParts);
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        for (Part part : addParts) {
            if (repo.lookupPart(part.getPartId()+"") == null) {
                throw new ValidationException("Product has a nonexistent part.");
            }
        }
        repo.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    public Part lookupPart(String prefix, int index) {
        return repo.lookupPart(prefix, index);
    }

    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    public Product lookupProduct(String prefix, int index) {
        return repo.lookupProduct(prefix, index);
    }

    public void updateInhousePart(int partIndex, InhousePart inhousePart) throws ValidationException {
        Validator.validatePart(inhousePart.getName(), inhousePart.getPrice(), inhousePart.getInStock(), inhousePart.getMin(), inhousePart.getMax());
        repo.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, OutsourcedPart outsourcedPart) throws ValidationException {
        Validator.validatePart(outsourcedPart.getName(), outsourcedPart.getPrice(), outsourcedPart.getInStock(), outsourcedPart.getMin(), outsourcedPart.getMax());
        repo.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, Product product) throws ValidationException {
        Validator.validateProduct(product.getName(), product.getPrice(), product.getInStock(), product.getMin(), product.getMax(), product.getAssociatedParts());
        repo.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        repo.deletePart(part);
    }

    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

}
