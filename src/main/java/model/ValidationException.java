package model;

public class ValidationException extends Exception {
    private final String msg;

    @Override
    public String getMessage() {return msg;}

    public ValidationException(String msg){
        this.msg = msg;
    }
}
