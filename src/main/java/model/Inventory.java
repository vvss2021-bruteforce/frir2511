
package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Iterator;

public class Inventory {
    
    // Declare fields
    private ObservableList<Product> products;
    private ObservableList<Part> allParts;
    private int autoPartId;
    private int autoProductId;


    public Inventory(){
        this.products = FXCollections.observableArrayList();
        this.allParts= FXCollections.observableArrayList();
        this.autoProductId=0;
        this.autoPartId=0;
    }


    // Declare methods
    /**
     * Add new product to observable list products
     * @param product 
     */
    public void addProduct(Product product) {
        products.add(product);
    }
    
    /**
     * Remove product from observable list products
     * @param product 
     */
    public void removeProduct(Product product) {
        products.remove(product);
    }
    
    /**
     * Accepts search parameter and if an ID or name matches input, that product is returned
     * @param searchItem
     * @return 
     */
    public Product lookupProduct(String searchItem) {
        boolean isFound = false;
        Iterator<Product> it = products.iterator();
        while (it.hasNext()) {
            Product p = it.next();
            if(p.getName().contains(searchItem))
                return p;
            else if ((p.getProductId()+"").equals(searchItem))
                return p;  // avem produse si gasim produsul cautat
            isFound = true;
        }
        if(!isFound) {
            return new Product(0, null, 0.0, 0, 0, 0, null);    // nu avem produse in lista
        }
        return null;    // cand avem produse dar nu gasim produsul cautat
    }

    /**
     * Accepts search parameter and if an ID or name matches input, the product of index index is returned
     * @param searchItem
     * @param index
     * @return
     */
    public Product lookupProduct(String searchItem, int index) {
        boolean isFound = false;
        for(Product p: products) {
            if(p.getName().contains(searchItem) || (p.getProductId()+"").equals(searchItem)) {
                index--;
                if (index == 0)
                    return p;
            }

            isFound = true;
        }
        if(!isFound) {
            return new Product(0, null, 0.0, 0, 0, 0, null);
        }
        return null;
    }


    /**
     * Update product at given index
     * @param index
     * @param product 
     */
    public void updateProduct(int index, Product product) {
        products.set(index, product);
    }
    
    /**
     * Getter for Product Observable List
     * @return 
     */
    public ObservableList<Product> getProducts() {
        return products;
    }

    public void setProducts(ObservableList<Product> list) {
        products=list;
    }
    
    /**
     * Add new part to observable list allParts
     * @param part 
     */
    public void addPart(Part part) {
        allParts.add(part);
    }
    
    /**
     * Removes part passed as parameter from allParts
     * @param part 
     */
    public void deletePart(Part part) {
        allParts.remove(part);
    }
    
    /**
     * Accepts search parameter and if an ID or name matches input, that part is returned
     * @param searchItem
     * @return 
     */
    public Part lookupPart(String searchItem) {
        for(Part p:allParts) {
            if(p.getName().contains(searchItem) || (p.getPartId()+"").equals(searchItem)) return p;
        }
        return null;
    }

    /**
     * Accepts search parameter and if an ID or name matches input, the part of index index is returned
     * @param searchItem
     * @param index
     * @return
     */
    public Part lookupPart(String searchItem, int index) {
        for(Part p:allParts) {
            if(p.getName().toLowerCase().contains(searchItem.toLowerCase()) || (p.getPartId()+"").equalsIgnoreCase(searchItem)) {
                index--;
                if (index == 0)
                    return p;
            }
        }
        return null;
    }
    
    /**
     * Update part at given index
     * @param index
     * @param part 
     */
    public void updatePart(int index, Part part) {
        allParts.set(index, part);
    }
    
    /**
     * Getter for allParts Observable List
     * @return 
     */
    public ObservableList<Part> getAllParts() {
        return allParts;
    }

    /**
     *
     * @param list
     */
    public void setAllParts(ObservableList<Part> list) {
        allParts=list;
    }
    
    /**
     * Method for incrementing part ID to be used to automatically
     * assign ID numbers to parts
     * @return 
     */
    public int getAutoPartId() {
        autoPartId++;
        return autoPartId;
    }
    
    /**
     * Method for incrementing product ID to be used to automatically
     * assign ID numbers to products
     * @return 
     */
    public int getAutoProductId() {
        autoProductId++;
        return autoProductId;
    }


    public void setAutoPartId(int id){
        autoPartId=id;
    }

    public void setAutoProductId(int id){
        autoProductId=id;
    }
    
}
