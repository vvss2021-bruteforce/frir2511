import repository.InventoryRepository;
import service.InventoryService;
import controller.MainScreenController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.util.logging.Logger;


public class Main extends Application {

    private Logger logger = Logger.getLogger(Main.class.getName());

    @Override
    public void start(Stage stage) throws Exception {

        InventoryRepository repo= new InventoryRepository(new File("src/main/resources/data/items.txt"));
        InventoryService service = new InventoryService(repo);
        logger.info("products : " + service.getAllProducts());
        logger.info("parts : " + service.getAllParts());
        FXMLLoader loader= new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));

        Parent root=loader.load();
        MainScreenController ctrl=loader.getController();
        ctrl.setService(service);

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
